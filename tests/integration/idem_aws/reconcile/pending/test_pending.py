def test_pending_default_impl(hub):
    # Test default implementation, pending if result is false or there are changes

    ret_pending1 = dict(changes={"a": "b"}, result=True)
    ret_pending2 = dict(changes=None, result=False)
    ret_not_pending = dict(changes=None, result=True)

    # State w/o is_pending implemented
    state = "aws.ec2.subnet"

    result = hub.reconcile.pending["aws"].is_pending(ret_pending1, state)
    assert result is True, "Expected pending because there are changes"

    result = hub.reconcile.pending["aws"].is_pending(ret_pending2, state)
    assert result is True, "Expected pending because the result is False"

    result = hub.reconcile.pending["aws"].is_pending(ret_not_pending, state)
    assert result is False, "Expected not pending"


def test_pending_custom_impl(hub, mock_hub):
    # Test customized implementation of the pending plugin,
    # based on new_state values.
    ret_pending = dict(new_state={"status": "updating"}, result=True)
    ret_not_pending = dict(new_state={"status": "complete"}, result=True)

    state = "aws.test"
    mock_hub.reconcile.pending["aws"].is_pending = hub.reconcile.pending[
        "aws"
    ].is_pending
    mock_hub.states[state].is_pending = _is_pending

    result = mock_hub.reconcile.pending["aws"].is_pending(ret_pending, state)
    assert result is True, "Expected pending"

    result = mock_hub.reconcile.pending["aws"].is_pending(ret_not_pending, state)
    assert result is False, "Expected not pending"


def _is_pending(ret: dict):
    return ret["new_state"].get("status") == "updating"
