import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_metric_alarm(hub, ctx):
    metric_alarm_temp_name = "idem-test-metric-alarm-" + str(uuid.uuid4())
    alarm_description = "description"
    ok_actions = []
    actions_enabled = True
    metric_name = "CPUUtilization_test"
    namespace = "AWS/EC2"
    statistic = "Minimum"
    extended_statistic = None
    # For testing added dummy instance id
    dimensions = [{"Name": "InstanceId", "Value": "i-0aaab28e6c0c771d7"}]
    period = 60
    evaluation_periods = 1
    datapoints_to_alarm = 1
    threshold = 0.1
    comparison_operator = "GreaterThanThreshold"
    threshold_metric_id = ""
    tags = {"alarm": metric_alarm_temp_name}
    alarm_actions = [
        f"arn:aws:swf:{ctx['acct'].get('region_name')}:460671877902:action/actions/AWS_EC2.InstanceId.Stop/1.0"
    ]
    insufficient_data_actions = [
        f"arn:aws:automate:{ctx['acct'].get('region_name')}:ec2:stop"
    ]

    # Create metric alarm with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.cloudwatch.metric_alarm.present(
        test_ctx,
        name=metric_alarm_temp_name,
        alarm_description=alarm_description,
        ok_actions=ok_actions,
        actions_enabled=actions_enabled,
        alarm_actions=alarm_actions,
        insufficient_data_actions=insufficient_data_actions,
        metric_name=metric_name,
        namespace=namespace,
        statistic=statistic,
        extended_statistic=extended_statistic,
        dimensions=dimensions,
        period=period,
        evaluation_periods=evaluation_periods,
        datapoints_to_alarm=datapoints_to_alarm,
        threshold=threshold,
        comparison_operator=comparison_operator,
        threshold_metric_id=threshold_metric_id,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert (
        f"Would create aws.cloudwatch.metric_alarm '{metric_alarm_temp_name}'"
        in ret["comment"]
    )
    resource = ret.get("new_state")
    assert ok_actions == resource.get("ok_actions")
    assert insufficient_data_actions == resource.get("insufficient_data_actions")
    assert namespace == resource.get("namespace")
    assert_metric_alarm(
        hub,
        resource,
        metric_alarm_temp_name,
        alarm_description,
        actions_enabled,
        alarm_actions,
        metric_name,
        statistic,
        dimensions,
        period,
        evaluation_periods,
        datapoints_to_alarm,
        threshold,
        comparison_operator,
        tags,
    )

    # Create metric alarm in real
    ret = await hub.states.aws.cloudwatch.metric_alarm.present(
        ctx,
        name=metric_alarm_temp_name,
        alarm_description=alarm_description,
        ok_actions=ok_actions,
        actions_enabled=actions_enabled,
        alarm_actions=alarm_actions,
        insufficient_data_actions=insufficient_data_actions,
        metric_name=metric_name,
        namespace=namespace,
        statistic=statistic,
        extended_statistic=extended_statistic,
        dimensions=dimensions,
        period=period,
        evaluation_periods=evaluation_periods,
        datapoints_to_alarm=datapoints_to_alarm,
        threshold=threshold,
        comparison_operator=comparison_operator,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Created aws.cloudwatch.metric_alarm '{metric_alarm_temp_name}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert ok_actions == resource.get("ok_actions")
    assert insufficient_data_actions == resource.get("insufficient_data_actions")
    assert namespace == resource.get("namespace")
    assert_metric_alarm(
        hub,
        resource,
        metric_alarm_temp_name,
        alarm_description,
        actions_enabled,
        alarm_actions,
        metric_name,
        statistic,
        dimensions,
        period,
        evaluation_periods,
        datapoints_to_alarm,
        threshold,
        comparison_operator,
        tags,
    )
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")

    # Describe metric alarm
    describe_ret = await hub.states.aws.cloudwatch.metric_alarm.describe(ctx)
    assert resource_id in describe_ret
    assert "aws.cloudwatch.metric_alarm.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "aws.cloudwatch.metric_alarm.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    described_resource_map["name"] = resource.get("name")
    assert ok_actions == resource.get("ok_actions")
    assert insufficient_data_actions == resource.get("insufficient_data_actions")
    assert namespace == resource.get("namespace")
    assert_metric_alarm(
        hub,
        described_resource_map,
        metric_alarm_temp_name,
        alarm_description,
        actions_enabled,
        alarm_actions,
        metric_name,
        statistic,
        dimensions,
        period,
        evaluation_periods,
        datapoints_to_alarm,
        threshold,
        comparison_operator,
        tags,
    )

    alarm_description = "test description by update"
    metric_name = "CPUUtilization_update"
    period = 120
    evaluation_periods = 2
    statistic = "Average"
    dimensions = [{"Name": "InstanceId", "Value": "i-0aaab28e6c0c771d8"}]
    datapoints_to_alarm = 2
    threshold = 0.5
    comparison_operator = "LessThanThreshold"
    tags = {"alarm_updated": metric_alarm_temp_name, "Type": "EC2"}

    alarm_actions = [
        f"arn:aws:swf:{ctx['acct'].get('region_name')}:460671877902:action/actions/AWS_EC2.InstanceId.Terminate/1.0"
    ]

    # Update metric alarm with test flag
    ret = await hub.states.aws.cloudwatch.metric_alarm.present(
        test_ctx,
        name=metric_alarm_temp_name,
        resource_id=resource_id,
        alarm_description=alarm_description,
        actions_enabled=actions_enabled,
        alarm_actions=alarm_actions,
        metric_name=metric_name,
        namespace=namespace,
        statistic=statistic,
        dimensions=dimensions,
        period=period,
        evaluation_periods=evaluation_periods,
        datapoints_to_alarm=datapoints_to_alarm,
        threshold=threshold,
        comparison_operator=comparison_operator,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_metric_alarm(
        hub,
        resource,
        metric_alarm_temp_name,
        alarm_description,
        actions_enabled,
        alarm_actions,
        metric_name,
        statistic,
        dimensions,
        period,
        evaluation_periods,
        datapoints_to_alarm,
        threshold,
        comparison_operator,
        tags,
    )

    # Update metric alarm in real
    ret = await hub.states.aws.cloudwatch.metric_alarm.present(
        ctx,
        name=metric_alarm_temp_name,
        resource_id=resource_id,
        alarm_description=alarm_description,
        actions_enabled=actions_enabled,
        alarm_actions=alarm_actions,
        metric_name=metric_name,
        namespace=namespace,
        statistic=statistic,
        dimensions=dimensions,
        period=period,
        evaluation_periods=evaluation_periods,
        datapoints_to_alarm=datapoints_to_alarm,
        threshold=threshold,
        comparison_operator=comparison_operator,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_metric_alarm(
        hub,
        resource,
        metric_alarm_temp_name,
        alarm_description,
        actions_enabled,
        alarm_actions,
        metric_name,
        statistic,
        dimensions,
        period,
        evaluation_periods,
        datapoints_to_alarm,
        threshold,
        comparison_operator,
        tags,
    )

    # Update metric alarm in real with no tags passed
    ret = await hub.states.aws.cloudwatch.metric_alarm.present(
        ctx,
        name=metric_alarm_temp_name,
        resource_id=resource_id,
        alarm_description=alarm_description,
        actions_enabled=actions_enabled,
        alarm_actions=alarm_actions,
        metric_name=metric_name,
        namespace=namespace,
        statistic=statistic,
        dimensions=dimensions,
        period=period,
        evaluation_periods=evaluation_periods,
        datapoints_to_alarm=datapoints_to_alarm,
        threshold=threshold,
        comparison_operator=comparison_operator,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_metric_alarm(
        hub,
        resource,
        metric_alarm_temp_name,
        alarm_description,
        actions_enabled,
        alarm_actions,
        metric_name,
        statistic,
        dimensions,
        period,
        evaluation_periods,
        datapoints_to_alarm,
        threshold,
        comparison_operator,
        tags,
    )

    # Update metric alarm in real with tags == empty
    ret = await hub.states.aws.cloudwatch.metric_alarm.present(
        ctx,
        name=metric_alarm_temp_name,
        resource_id=resource_id,
        alarm_description=alarm_description,
        actions_enabled=actions_enabled,
        alarm_actions=alarm_actions,
        metric_name=metric_name,
        namespace=namespace,
        statistic=statistic,
        dimensions=dimensions,
        period=period,
        evaluation_periods=evaluation_periods,
        datapoints_to_alarm=datapoints_to_alarm,
        threshold=threshold,
        comparison_operator=comparison_operator,
        tags=[],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_metric_alarm(
        hub,
        resource,
        metric_alarm_temp_name,
        alarm_description,
        actions_enabled,
        alarm_actions,
        metric_name,
        statistic,
        dimensions,
        period,
        evaluation_periods,
        datapoints_to_alarm,
        threshold,
        comparison_operator,
        {},
    )

    # Delete metric alarm with test flag
    ret = await hub.states.aws.cloudwatch.metric_alarm.absent(
        test_ctx, name=metric_alarm_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.cloudwatch.metric_alarm", name=metric_alarm_temp_name
        )[0]
        in ret["comment"]
    )

    # Delete metric alarm in real
    ret = await hub.states.aws.cloudwatch.metric_alarm.absent(
        ctx, name=metric_alarm_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.cloudwatch.metric_alarm", name=metric_alarm_temp_name
        )[0]
        in ret["comment"]
    )

    # Delete already deleted metric alarm
    ret = await hub.states.aws.cloudwatch.metric_alarm.absent(
        ctx, name=metric_alarm_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.cloudwatch.metric_alarm", name=metric_alarm_temp_name
        )[0]
        in ret["comment"]
    )


def assert_metric_alarm(
    hub,
    resource,
    metric_alarm_temp_name,
    alarm_description,
    actions_enabled,
    alarm_actions,
    metric_name,
    statistic,
    dimensions,
    period,
    evaluation_periods,
    datapoints_to_alarm,
    threshold,
    comparison_operator,
    tags,
):
    assert metric_alarm_temp_name == resource.get("name")
    assert alarm_description == resource.get("alarm_description")
    assert actions_enabled == resource.get("actions_enabled")
    assert alarm_actions == resource.get("alarm_actions")
    assert metric_name == resource.get("metric_name")
    assert statistic == resource.get("statistic")
    assert dimensions == resource.get("dimensions")
    assert period == resource.get("period")
    assert evaluation_periods == resource.get("evaluation_periods")
    assert datapoints_to_alarm == resource.get("datapoints_to_alarm")
    assert threshold == resource.get("threshold")
    assert comparison_operator == resource.get("comparison_operator")
    assert tags == resource.get("tags")
