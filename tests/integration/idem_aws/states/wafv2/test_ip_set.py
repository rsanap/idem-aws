import copy
import time
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "test-wafv2-ip-set-" + str(int(time.time())),
    "ip_address_version": "IPV4",
    "addresses": ["1.0.7.0/32", "19.1.2.44/32", "2.0.0.0/24"],
    "scope": "REGIONAL",
    "description": "for testing idem plugin for ip_set",
}
comment_utils_kwargs = {
    "resource_type": "aws.wafv2.ip_set",
    "name": PARAMETER["name"],
}
# To run the tests locally comment out all @pytest.mark.localstack(False)
# fixtures as wafv2 are not supported by localstack pro or localstack


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test):
    """Testing the creation of ip_set resource as per the desired parameter input on AWS."""
    global PARAMETER
    ctx["test"] = __test
    created_message = hub.tool.aws.comment_utils.create_comment(**comment_utils_kwargs)
    would_create_message = hub.tool.aws.comment_utils.would_create_comment(
        **comment_utils_kwargs
    )
    PARAMETER["tags"] = {"Name": PARAMETER["name"]}
    ret = await hub.states.aws.wafv2.ip_set.present(
        ctx,
        **PARAMETER,
    )
    resource = ret["new_state"]
    if __test:
        """Checking that a resource with given credentials doesnt exist so it is eligible for creation."""
        assert would_create_message == ret["comment"]
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        PARAMETER["lock_token"] = resource["lock_token"]
        """Checking the presence of all the desired parameters as was given by the user."""
        assert created_message == ret["comment"]
    assert PARAMETER["name"] == resource["name"]
    assert PARAMETER["scope"] == resource["scope"]
    assert PARAMETER["ip_address_version"] == resource["ip_address_version"]
    assert PARAMETER["tags"] == resource["tags"]
    assert PARAMETER["description"] == resource["description"]
    assert set(PARAMETER["addresses"]) == set(resource["addresses"])


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="get", depends=["present"])
async def test_get(hub, ctx):
    """Test to check if the created resource exists at the AWS site."""
    get_ret = await hub.exec.aws.wafv2.ip_set.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        scope="REGIONAL",
    )
    get_response = get_ret["ret"]
    """Asserting that the get call to the newly created ip_set resource was successful"""
    assert get_ret["result"] and not get_ret["comment"]
    """Asserting that all the parameters as prescribed by the user were used in creating the new ip_set."""
    assert PARAMETER["name"] == get_response["name"]
    assert PARAMETER["resource_id"] == get_response["resource_id"]
    assert PARAMETER["ip_address_version"] == get_response["ip_address_version"]
    assert PARAMETER["scope"] == get_response["scope"]
    assert set(PARAMETER["addresses"]) == set(get_response["addresses"])
    assert PARAMETER["description"] == get_response["description"]
    assert PARAMETER["tags"] == get_response["tags"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update", depends=["present"])
async def test_update_parameters(hub, ctx, __test):
    """Test to update all the updatable parameters of the ip_set resource."""
    desired_state = {
        "name": PARAMETER["name"],
        "scope": PARAMETER["scope"],
        "resource_id": PARAMETER["resource_id"],
        "description": "Updating addresses for the ip_set",
        "addresses": [
            "1.0.7.0/32",
            "19.1.2.44/32",
            "2.0.0.0/24",
            "192.0.2.44/32",
            "192.0.2.0/24",
            "198.0.2.0/24",
        ],
        "ip_address_version": "IPV4",
    }
    would_update_comment = hub.tool.aws.comment_utils.would_update_comment(
        **comment_utils_kwargs
    )
    update_comment = hub.tool.aws.comment_utils.update_comment(**comment_utils_kwargs)
    ctx["test"] = __test
    """Updating the created resource per new desired parameters."""
    update_ret = await hub.states.aws.wafv2.ip_set.present(
        ctx,
        **desired_state,
    )
    """Making sure that update was successful."""
    assert update_ret["result"]
    "Old state status"
    old_state_status = update_ret["old_state"]
    updated_status = update_ret["new_state"]
    assert old_state_status and updated_status

    assert old_state_status["scope"] == PARAMETER["scope"]
    assert old_state_status["name"] == PARAMETER["name"]
    assert old_state_status["resource_id"] == PARAMETER["resource_id"]
    assert old_state_status["description"] == PARAMETER["description"]
    assert set(old_state_status["addresses"]) == set(PARAMETER["addresses"])

    if __test:
        assert would_update_comment == update_ret["comment"]
    else:
        """Asserting that all the desired parameters as given by the desired_state is updated for the given resource
        name."""
        assert update_comment == update_ret["comment"]

    assert updated_status["name"] == updated_status["name"]
    assert updated_status["resource_id"] == updated_status["resource_id"]
    assert updated_status["scope"] == updated_status["scope"]
    assert updated_status["description"] == updated_status["description"]
    assert set(updated_status["addresses"]) == set(updated_status["addresses"])
    if not __test:
        PARAMETER["addresses"] = desired_state["addresses"]
        PARAMETER["scope"] = desired_state["scope"]
        PARAMETER["description"] = desired_state["description"]
        PARAMETER["lock_token"] = update_ret["new_state"]["lock_token"]


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="describe", depends=["update"])
async def test_describe(hub, ctx):
    """Test to describe the existence and details of all the ip_set resources and assert the details of current resource
    as desired."""
    describe_ret = await hub.states.aws.wafv2.ip_set.describe(ctx)
    name = PARAMETER["name"]

    """Asserting that the resource name as created in this context is available at the AWS site."""
    assert name in describe_ret
    assert "aws.wafv2.ip_set.present" in describe_ret[name]

    resource = dict(ChainMap(*describe_ret[name].get("aws.wafv2.ip_set.present")))

    """Asserting that the resource name is described with the details of tags, locktoken, ip address version,
    addresses scope and resource id as has been created and updated in the context of this integration test in the AWS
    site."""
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["tags"] == resource["tags"]
    assert PARAMETER["lock_token"] == resource.get("lock_token")
    assert PARAMETER["ip_address_version"] == resource.get("ip_address_version")
    assert PARAMETER["description"] == resource.get("description")
    assert PARAMETER["scope"] == resource["scope"]
    assert set(PARAMETER["addresses"]) == set(resource["addresses"])
    assert PARAMETER["resource_id"] == resource["resource_id"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="add_ip_set_tag", depends=["describe"])
async def test_add_ip_set_tag(hub, ctx, __test):
    """Test to update or add a tag to an existing resource."""
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    timestamp = str(int(time.time()))
    new_parameter["tags"].update(
        {
            "New-Tag": f"idem-test-value-{timestamp}",
        }
    )
    """Updating the state of tags as per the new requirement given by new_parameter["tags"]."""
    ret = await hub.states.aws.wafv2.ip_set.present(ctx, **new_parameter)
    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource["name"]
    assert PARAMETER["resource_id"] == old_resource["resource_id"]
    assert PARAMETER["scope"] == old_resource["scope"]
    assert PARAMETER["ip_address_version"] == old_resource["ip_address_version"]
    assert PARAMETER["tags"] == old_resource["tags"]
    assert PARAMETER["description"] == old_resource["description"]
    assert set(PARAMETER["addresses"]) == set(old_resource["addresses"])

    resource = ret["new_state"]
    assert new_parameter["name"] == resource["name"]
    assert new_parameter["resource_id"] == resource["resource_id"]
    assert new_parameter["scope"] == resource["scope"]
    assert new_parameter["ip_address_version"] == resource["ip_address_version"]
    assert new_parameter["tags"] == resource.get("tags")
    assert new_parameter["description"] == resource["description"]

    assert ret["new_state"]
    assert ret["old_state"]
    added = {
        tag: new_parameter["tags"][tag]
        for tag in new_parameter["tags"]
        if tag not in PARAMETER["tags"]
    }
    removed = {
        tag: PARAMETER["tags"][tag]
        for tag in PARAMETER["tags"]
        if tag not in new_parameter["tags"]
    }
    """Asserting that the update request went through successfully."""
    assert ret["result"], ret["comment"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.already_exists_comment(**comment_utils_kwargs)
            + hub.tool.aws.comment_utils.would_update_tags_comment(removed, added)
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.already_exists_comment(**comment_utils_kwargs)
            + hub.tool.aws.comment_utils.update_tags_comment(removed, added)
            == ret["comment"]
        )
    if not __test:
        PARAMETER["tags"] = new_parameter["tags"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="remove_old_ipset_tag", depends=["add_ip_set_tag"])
async def test_remove_old_ip_set_tag(hub, ctx, __test):
    """Test to delete only the tags associated with the resource."""
    global PARAMETER
    ctx["test"] = __test

    new_parameter = copy.deepcopy(PARAMETER)
    del new_parameter["tags"]["New-Tag"]

    """Removing the existing tags"""
    ret = await hub.states.aws.wafv2.ip_set.present(ctx, **new_parameter)

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")

    old_resource = ret["old_state"]

    """Assertions to make sure undesired changes to any other parameters are not done."""
    assert PARAMETER["scope"] == old_resource["scope"]
    assert PARAMETER["name"] == old_resource["name"]
    assert PARAMETER["ip_address_version"] == old_resource["ip_address_version"]
    assert PARAMETER["description"] == old_resource["description"]
    assert PARAMETER["tags"] == old_resource["tags"]
    assert set(PARAMETER["addresses"]) == set(old_resource["addresses"])

    new_resource = ret["new_state"]
    assert new_parameter["scope"] == new_resource["scope"]
    assert new_parameter["name"] == new_resource["name"]
    assert new_parameter["ip_address_version"] == new_resource["ip_address_version"]
    assert new_parameter["description"] == new_resource["description"]
    assert set(new_parameter["addresses"]) == set(new_resource["addresses"])
    assert new_parameter["tags"] == new_resource.get("tags")

    added = {
        tag: new_parameter["tags"][tag]
        for tag in new_parameter["tags"]
        if tag not in PARAMETER["tags"]
    }
    removed = {
        tag: PARAMETER["tags"][tag]
        for tag in PARAMETER["tags"]
        if tag not in new_parameter["tags"]
    }
    if __test:
        assert (
            hub.tool.aws.comment_utils.already_exists_comment(**comment_utils_kwargs)
            + hub.tool.aws.comment_utils.would_update_tags_comment(removed, added)
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.already_exists_comment(**comment_utils_kwargs)
            + hub.tool.aws.comment_utils.update_tags_comment(removed, added)
            == ret["comment"]
        )

    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="absent", depends=["remove_old_ipset_tag"])
async def test_absent(hub, ctx, __test):
    """Test to delete and assert that deletion request went through."""
    global PARAMETER
    ctx["test"] = __test
    ret = await hub.states.aws.wafv2.ip_set.absent(
        ctx,
        name=PARAMETER["name"],
        scope=PARAMETER["scope"],
        resource_id=PARAMETER["resource_id"],
    )
    """Asserting that the test ip_set resource created within the context of this test was deleted successfully."""
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(**comment_utils_kwargs)
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(**comment_utils_kwargs)
            == ret["comment"]
        )
    resource = ret.get("old_state")
    """Verifying that the deleted resource was the same as was created within the context of this set of test."""
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert PARAMETER["ip_address_version"] == resource.get("ip_address_version")
    assert PARAMETER["scope"] == resource.get("scope")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["description"] == resource.get("description")
    assert set(PARAMETER["addresses"]) == set(resource.get("addresses"))


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    """Test to verify if an already deleted resource has any trace left using its name."""
    global PARAMETER
    ctx["test"] = __test
    ret = await hub.states.aws.wafv2.ip_set.absent(
        ctx,
        name=PARAMETER["name"],
        scope=PARAMETER["scope"],
        resource_id=PARAMETER["resource_id"],
    )
    already_absent_comment = hub.tool.aws.comment_utils.already_absent_comment(
        **comment_utils_kwargs
    )
    """Asserting that the resource has no trace left by making sure neither the old or new state in any form for this
    resource exists."""
    assert already_absent_comment == ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert ret["result"]


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    """This is a cleanup to restore the testing environment to its original state."""
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.wafv2.ip_set.absent(
            ctx,
            name=PARAMETER["name"],
            scope=PARAMETER["scope"],
            resource_id=PARAMETER["resource_id"],
        )
        assert ret["result"], ret["comment"]
        assert (not ret["old_state"]) and (not ret["new_state"])
