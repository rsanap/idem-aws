import copy
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_delivery_channel(hub, ctx, aws_config_recorder_delivery_channel):
    if hub.tool.utils.is_running_localstack(ctx):
        return

    # Config Recorder and Delivery Channel is already created in fixture -> states/conftest.py
    # This is testing update scenario in real and with test flag.
    config_recorder_name = aws_config_recorder_delivery_channel.get("name")
    assert config_recorder_name
    # Copy ctx to test with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    resource_name = "delivery-channel"
    delivery_freq = "One_Hour"
    config_snapshot_delivery_properties_to_update = {
        "delivery_frequency": "Three_Hours"
    }

    # Describe delivery_channel in real
    describe_ret = await hub.states.aws.config.delivery_channel.describe(ctx)
    assert describe_ret
    assert "aws.config.delivery_channel.present" in describe_ret.get(resource_name)
    described_resource = describe_ret.get(resource_name).get(
        "aws.config.delivery_channel.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert "name" in described_resource_map
    assert "resource_id" in described_resource_map
    assert "sns_topic_arn" in described_resource_map
    assert "s3_bucket_name" in described_resource_map
    assert "config_snapshot_delivery_properties" in described_resource_map
    delivery_properties = described_resource_map["config_snapshot_delivery_properties"]
    assert delivery_freq == delivery_properties["delivery_frequency"]
    assert resource_name == described_resource_map["resource_id"]

    s3_bucket_name = described_resource_map.get("s3_bucket_name")
    sns_topic_arn = described_resource_map.get("sns_topic_arn")

    # Update delivery-channel with test flag
    ret = await hub.states.aws.config.delivery_channel.present(
        test_ctx,
        name=resource_name,
        s3_bucket_name=s3_bucket_name,
        sns_topic_arn=sns_topic_arn,
        config_snapshot_delivery_properties=config_snapshot_delivery_properties_to_update,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would update aws.config.delivery_channel '{resource_name}'" in ret["comment"]
    )
    resource = ret.get("new_state")
    assert resource_name == resource.get("name")
    assert config_snapshot_delivery_properties_to_update.get(
        "delivery_frequency"
    ) == resource.get("config_snapshot_delivery_properties").get("delivery_frequency")
    assert described_resource_map.get("s3_bucket_name") == resource.get(
        "s3_bucket_name"
    )
    assert described_resource_map.get("sns_topic_arn") == resource.get("sns_topic_arn")
    # update delivery-channel in real
    ret = await hub.states.aws.config.delivery_channel.present(
        ctx,
        name=resource_name,
        s3_bucket_name=s3_bucket_name,
        sns_topic_arn=sns_topic_arn,
        config_snapshot_delivery_properties=config_snapshot_delivery_properties_to_update,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Updated aws.config.delivery_channel '{resource_name}'" in ret["comment"]

    # Delete delivery-channel with test flag
    ret = await hub.states.aws.config.delivery_channel.absent(
        test_ctx, name=resource_name
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert resource_name == ret["old_state"]["name"]
    assert s3_bucket_name == ret["old_state"]["s3_bucket_name"]
    assert sns_topic_arn == ret["old_state"]["sns_topic_arn"]
    assert (
        f"Would delete aws.config.delivery_channel '{resource_name}'" in ret["comment"]
    )
    # Describe delivery_channel in real
    describe_ret = await hub.states.aws.config.delivery_channel.describe(ctx)
    assert describe_ret
    assert "aws.config.delivery_channel.present" in describe_ret.get(resource_name)
    described_resource = describe_ret.get(resource_name).get(
        "aws.config.delivery_channel.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert "name" in described_resource_map
    assert "resource_id" in described_resource_map
    assert "sns_topic_arn" in described_resource_map
    assert "s3_bucket_name" in described_resource_map
    assert "config_snapshot_delivery_properties" in described_resource_map
    delivery_properties = described_resource_map["config_snapshot_delivery_properties"]
    assert delivery_properties["delivery_frequency"] == "Three_Hours"
