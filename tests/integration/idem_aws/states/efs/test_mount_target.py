import copy
import time
from collections import ChainMap

import pytest


@pytest.mark.localstack(
    False,
    "Localstack currently doesn't support EFS mount targets. tests added to run with actual AWS services",
)
@pytest.mark.asyncio
async def test_mount_target(
    hub,
    ctx,
    aws_efs_file_system,
    aws_ec2_subnet,
    aws_ec2_security_group,
    aws_ec2_security_group_2,
):
    name = "idem-test-fs-mount-target-" + str(int(time.time()))
    file_system_id = aws_efs_file_system["resource_id"]
    subnet_id = aws_ec2_subnet["SubnetId"]
    resource_id = None
    security_groups = [
        aws_ec2_security_group["resource_id"],
    ]

    updated_security_groups = [
        aws_ec2_security_group["resource_id"],
        aws_ec2_security_group_2["resource_id"],
    ]

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Test-run mount target create
    ret = await hub.states.aws.efs.mount_target.present(
        test_ctx,
        name=name,
        file_system_id=file_system_id,
        resource_id=resource_id,
        subnet_id=subnet_id,
        security_groups=security_groups,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == (f"Would create aws.efs.mount_target '{name}'",)
    resource = ret.get("new_state")
    assert file_system_id == resource["file_system_id"]
    assert subnet_id == resource["subnet_id"]
    assert set(security_groups) == set(resource["security_groups"])

    # Create actual mount target
    ret = await hub.states.aws.efs.mount_target.present(
        ctx,
        name=name,
        file_system_id=file_system_id,
        resource_id=resource_id,
        subnet_id=subnet_id,
        security_groups=security_groups,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == (f"Created aws.efs.mount_target '{name}'",)
    resource = ret.get("new_state")

    assert file_system_id == resource["file_system_id"]
    assert subnet_id == resource["subnet_id"]
    assert set(security_groups) == set(resource["security_groups"])

    # Populate Id of previously created mount target
    resource_id = resource["resource_id"]

    # Describe mount targets
    ret = await hub.states.aws.efs.mount_target.describe(ctx)
    resource = ret.get(resource_id).get("aws.efs.mount_target.present")
    resource_map = dict(ChainMap(*resource))
    assert resource_id == resource_map.get("name")
    assert resource_id == resource_map.get("resource_id")
    assert subnet_id == resource_map.get("subnet_id")
    assert set(security_groups) == set(resource_map.get("security_groups"))

    # Test-run mount target update with no changes
    ret = await hub.states.aws.efs.mount_target.present(
        test_ctx,
        name=name,
        file_system_id=file_system_id,
        resource_id=resource_id,
        subnet_id=subnet_id,
        security_groups=security_groups,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == (f"aws.efs.mount_target '{name}' already exists",)
    resource = ret.get("new_state")
    assert file_system_id == resource["file_system_id"]
    assert subnet_id == resource["subnet_id"]
    assert set(security_groups) == set(resource["security_groups"])

    # Update actual mount target with no changes
    ret = await hub.states.aws.efs.mount_target.present(
        ctx,
        name=name,
        file_system_id=file_system_id,
        resource_id=resource_id,
        subnet_id=subnet_id,
        security_groups=security_groups,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == (f"aws.efs.mount_target '{name}' already exists",)
    resource = ret.get("new_state")
    assert file_system_id == resource["file_system_id"]
    assert subnet_id == resource["subnet_id"]
    assert set(security_groups) == set(resource["security_groups"])

    # Test-run mount target update with changes
    ret = await hub.states.aws.efs.mount_target.present(
        test_ctx,
        name=name,
        file_system_id=file_system_id,
        resource_id=resource_id,
        subnet_id=subnet_id,
        security_groups=[],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == (f"Would update aws.efs.mount_target '{name}'",)
    resource = ret.get("new_state")
    assert file_system_id == resource["file_system_id"]
    assert subnet_id == resource["subnet_id"]
    assert [] == resource["security_groups"]

    # Update actual mount target security group with changes
    ret = await hub.states.aws.efs.mount_target.present(
        ctx,
        name=name,
        file_system_id=file_system_id,
        resource_id=resource_id,
        subnet_id=subnet_id,
        security_groups=updated_security_groups,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == (
        f"Updated security groups for aws.efs.mount_target '{name}'",
    )
    resource = ret.get("new_state")
    assert file_system_id == resource["file_system_id"]
    assert subnet_id == resource["subnet_id"]
    assert set(updated_security_groups) == set(resource["security_groups"])

    # Test-run mount target deletion
    ret = await hub.states.aws.efs.mount_target.absent(
        test_ctx, name=name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert ret["comment"] == (f"Would delete aws.efs.mount_target '{name}'",)
    resource = ret.get("old_state")
    assert file_system_id == resource["file_system_id"]
    assert subnet_id == resource["subnet_id"]
    assert set(updated_security_groups) == set(resource["security_groups"])

    # Delete non-existent mount target
    ret = await hub.states.aws.efs.mount_target.absent(
        ctx, name=name, resource_id="dummy"
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
    assert ret["comment"] == (f"aws.efs.mount_target '{name}' already absent",)

    # Delete mount target
    ret = await hub.states.aws.efs.mount_target.absent(
        ctx, name=name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert ret["comment"] == (f"Deleted aws.efs.mount_target '{name}'",)
    resource = ret.get("old_state")
    assert file_system_id == resource["file_system_id"]
    assert subnet_id == resource["subnet_id"]
    assert set(updated_security_groups) == set(resource["security_groups"])
