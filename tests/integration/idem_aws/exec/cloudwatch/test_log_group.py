import time
from typing import List

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get(hub, ctx, aws_cloudwatch_log_group):
    log_group_get_name = "idem-test-exec-get-log_group-" + str(int(time.time()))
    ret = await hub.exec.aws.cloudwatch.log_group.get(
        ctx,
        name=log_group_get_name,
        resource_id=aws_cloudwatch_log_group["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert aws_cloudwatch_log_group["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get_invalid_resource_id(hub, ctx):
    log_group_get_name = "idem-test-exec-get-log_group-" + str(int(time.time()))
    ret = await hub.exec.aws.cloudwatch.log_group.get(
        ctx,
        name=log_group_get_name,
        resource_id="@Fake-id",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert (
        f"Get aws.cloudwatch.log_group '{log_group_get_name}' result is empty"
        in str(ret["comment"])
    )


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_list(hub, ctx, aws_cloudwatch_log_group):
    log_group_list_name = "idem-test-exec-list-log_group-" + str(int(time.time()))
    ret = await hub.exec.aws.cloudwatch.log_group.list(ctx, name=log_group_list_name)
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    resource_id_list = []
    for log_group in ret["ret"]:
        resource_id_list.append(log_group["resource_id"])
    if aws_cloudwatch_log_group["resource_id"] not in resource_id_list:
        assert False
    else:
        assert True
